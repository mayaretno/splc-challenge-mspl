\label{sec:casestudyjava} % Subsection: Implementing Case Study in Java (Sec. 2.4)

As an OO modeling language, UML to Java code transformation is straightforward.
In this challenge, transforming the proposed UML diagram with the UML-DOP profile to
Java code requires some experiments. The generated Java code should
enable the delta application behavior and multi-variant interoperability.
Thus, we use the Gradle build automation tool to assist the compilation process.

We suggest a two-stages process. First, we create a Gradle multi-project that represents
core modules and delta modules modeled as Gradle projects. Second, we implement
core modules and delta modules as Java packages in its respective Gradle
project. At the end of process, we define Java main classes that simulate product
generation in the product line. The complete code can be found at URL\@:
\url{https://gitlab.com/mayaretno/splc-challenge-mspl/tree/master/uml-to-java}.

\subsubsection{Initializing Product Lines as a Gradle Project} % Sec. 2.4.1
\label{sec:initgradle}

The process starts by creating a Gradle multi-project that serves as a root
project by using Gradle initialization script. The root project is used to
contain nested Gradle projects that represent core modules and delta modules
of each product line. The product lines from challenge description are
initialised into seven Gradle subprojects that comprise three core modules
and four delta modules. Core modules contain implementation of core product and
base code for implementing Factory and Decorator design patterns. Delta modules
contain Java code that simulate delta application by decorator classes from
related core modules. Both core and delta modules have Gradle build script
that define dependencies between modules during compile-time and Java code
for simulating product configuration and generation by using factory classes.

% Each module is initialized by using Gradle initialization script in similar
% fashion to initialization of root project.

% For example, suppose that we have product lines for \texttt{SignalLine},
% \texttt{SwitchLine}, and \texttt{StationLine}. Both \texttt{SignalLine} and
% \texttt{SwitchLine} have a core module and two delta modules while
% \texttt{StationLine} only contain core module. Within each of those product lines,
% there is a Java class that handles
% product configuration and generation. Each product line
% and its relationship with others are represented as a Gradle
% subproject with a build script. Product configuration and specification of
% each product line are represented as Java classes in their respective Gradle
% subproject. 

%
%The following code snippets describes a shell script used for
%creating a root Gradle multi-project followed by initialization of each
%product line as Gradle subprojects.
%
%\begin{bashcode}
%gradle init --type=basic
%mkdir signalpl-core signalpl-main signalpl-pre switchpl-core switchpl-electric switchpl-manual stationpl
%for sp in $(ls | grep pl); do pushd $sp && gradle init --type=java && popd && echo "include '$sp'" >> settings.gradle; done
%\end{bashcode}

A variant may depend on another product that belongs to the same
or a different product line. In such a scenario, we use the dependency
configuration provided by Gradle to import and link the required dependency
during compile-time. The dependency configuration is derived by identifying 
variables and arguments that contain types belong to different product lines.
If such dependency relationship is present, then the dependency configuration
is set to include the Gradle subproject that contains the dependent
interface/class.

For example, a class in the core module of \texttt{SwitchLine} requires a
reference to \texttt{Sig} type that declared in the core module of
\texttt{SignalLine}. This requirement is defined as a dependency in the Gradle
build script (\texttt{build.gradle}) of the project which contains the source code
artefacts for core module of \texttt{SwitchLine}. 
%In similar fashion, Gradle
%build script in \texttt{StationLine} specifies dependencies to Gradle
%subprojects that contains source code artefacts required for building
%\texttt{Station} product and its variants.
The following code snippets illustrates how a dependency to an another Gradle
project is declared in a Gradle build script.
 
\begin{gradlecode}
dependencies{ implementation project (":signalpl-core")}
\end{gradlecode}

% Since there is only one-to-one mapping between a delta with a feature
% in the case study, a delta module in Gradle project will contain the
% implementation of the variant.
\vspace{-1em}
\subsubsection{Preparing Core and Delta Modules} % Sec. 2.4.2
After initializing the Gradle subprojects, we prepare the core and delta modules
based on the UML diagram with UML-DOP profile. The UML class without stereotype
is transformed to core module. We start by creating core modules as a set of
Java classes and interfaces in a Java package. All standard and
\texttt{<<interface>>} stereotyped classes in the UML diagram are implemented as
Java classes and Java interfaces, respectively.

The UML class with specific stereotype, such as \texttt{<<modified Class>>} in Figure~\ref{fig:uml1},
is transformed into delta modules. To simulate the delta application for transforming core product
to its variants later in the pipeline, each class in core module that may have varying implementation
is designed to follow Decorator design pattern. 
% the product (core and variants) is represented as Java class.

For example, the core module of \texttt{SignalLine} is implemented by following
Decorator design pattern. One possible implementation of design pattern is
by creating three Java classes (\texttt{Signal}, \texttt{SignalComponent}, and
\texttt{SignalDecorator}) that serve as the base implementation and variation
point for \texttt{Signal} product in \texttt{SignalLine}. 

A variant of \texttt{Signal} product is then implemented by creating a Java
class which serves as a decorator. This class wraps the core product and
implements specific behaviour of the variant. According to the Decorator design
pattern, the actual instantiation process of the product and its wrapping
mechanism are defined as a factory class which follows the Factory design
pattern. The following code snippets illustrates how the factory class is used
to produce a variant of \texttt{Signal} product that requires \texttt{Main}
feature in \texttt{SignalLine}.

%\begin{javacode}
%package rse.signalpl.mainsig;
%import rse.signalpl.SignalFactory;
%import rse.signalpl.core.Sig;
%import rse.signalpl.core.SignalComponent;
%import rse.signalpl.core.SignalDecorator;
%
%\end{javacode}
\begin{javacode}
public class Signal extends SignalDecorator {
    public Signal(SignalComponent sig) { super(sig); }
    ...
    public static void main(String[] args) {
        Sig base = SignalFactory.createSignal();
        Sig mainSig = SignalFactory.createSignal(Signal.class.getName(), base);
    }
}
\end{javacode}

The delta modules for each feature in the product line are implemented as Java
packages. Each Java package contains a Java class that represent the resulting
product variant. We manually encoded the product variant as a decorator class
that wraps the core product and provides the modified implementation. We also
create a Java class for representing the product line by following Factory design
pattern to encapsulate product generation of the product line. The factory class is
then put into the core module of the product line represented as a Gradle project.

%\DA{bagian ini bisa diremove atau dipotong kodenya yang spesifik mau dibahas?}
%The following code snippets provides examples of the factory class in
%\texttt{SignalLine}:
%
%\begin{javacode}
%package rse.signalpl;
%import rse.signalpl.core.Sig;
%import rse.signalpl.core.Signal;
%
%public class SignalFactory {
%
%    public static Sig createSignal() { return new Signal(); }
%
%    public static Sig createSignal(String fullyQualifiedName, Object... base) {
%        Sig signal = (base.length == 0) ? null : (Sig) base[0];
%
%        try {
%            Class<?> clz = Class.forName(fullyQualifiedName);
%            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
%            signal = (Sig) constructor.newInstance(base);
%        } catch (Exception ex) {
%            System.err.println("Returning base Signal.");
%            System.err.println("Given FQN: " + fullyQualifiedName);
%        }
%        return signal;
%    }
%}
%\end{javacode}
\vspace{-0.8em}
\subsubsection{Product Generation in Java}  % Sec. 2.4.3

Based on Figure~\ref{fig:uml2}, \texttt{StationLine} uses variants from
\texttt{SignalLine} and \texttt{SwitchLine}. Product selection in ABS starts
from selecting the related features. In the diagram, \texttt{StationA} product
requires \texttt{Notification} feature and other features which present in other product
lines, such as \texttt{Main} and \texttt{Pre} from \texttt{SignalLine}. In Java,
this requirement is implemented by creating a Java class that represents
\texttt{StationLine}. The class defines product configuration and generation as methods.

% There is a method contains instructions to demonstrate product variant, such as
% \texttt{Station A} and \texttt{Station B}.

The following code snippets illustrates how a method named \texttt{createStationA}
calls factory classes from other product lines to obtain required features as
Java object. Objects from \texttt{SignalLine} and \texttt{SwitchLine} are instantiated
via factory classes from both product lines. Therefore, \texttt{createStationA} has
responsibilities to create and compose the required variants into the desired product. This
process is also assisted by Gradle by ensuring required classes specified in factory
classes are available in Java classpath during compile-time.

% We can see inside
% the method, there are objects from \texttt{Signal} and \texttt{Switch}
% product lines. Therefore, this method has responsibility to create 
% and compose required variants into desired product by utilizing factory classes
% from dependent product lines.  

%The following snippet code shows dependencies written in
%Gradle build script that part of \texttt{StationLine}'s artifacts. 
%
%\begin{gradlecode}
%dependencies {
%   implementation project(':signalpl-core')
%   implementation project(':signalpl-mainsig')
%   implementation project(':signalpl-presig')
%}
%\end{gradlecode}

%Furthermore, we create a Java class for creating and composing required variants into desired
%product by utilizing factory classes from dependent product lines. The following 
%
%%The following
%code snippets illustrates dependency management using Gradle and product
%generation using Java:

\begin{javacode}
private static Station createStationA() {
  Sig s1 = SignalFactory.createSignal("rse.signalpl.mainsig.Signal",SignalFactory.createSignal());
  Sig s2 = SignalFactory.createSignal("rse.signalpl.presig.Signal",SignalFactory.createSignal(),s1);
  Sw sw1 = SwitchFactory.createSwitch("rse.switchpl.manual.Switch",SwitchFactory.createSwitch());
  sw1.register(s1);
  Station stationA = new Station();
  stationA.addSignal(s1);
  stationA.addSignal(s2);
  stationA.addSwitches(sw1);
  stationA.addNotification(new Notification("test"));
  return stationA;
}
\end{javacode}

\subsubsection{Running Example - Summary} % Sec. 2.4.4
To provide a clearer picture on how the diagram is transformed to Java code, we
take product lines \texttt{StationLine}, \texttt{SignalLine}, and
\texttt{SwitchLine} modeled in Figure~\ref{fig:uml2}. We start by creating new
Gradle project and its subprojects as mentioned in~\ref{sec:initgradle}. We also
ensure exposed classes and types to have public visiblity modifier in its
corresponding Java classes and interfaces. For each subproject that represents a
delta module, we specify the dependencies in its Gradle build script to include
the Gradle subproject that contains required module. To create a product (either
core product or its variants), we use Gradle tasks to build and run a Java main
class in a product line. The Java main class is created in each Gradle subproject
to demonstrate the process for product configuration and generation. Finally, we
create \texttt{StationLine} that uses factory classes from dependent product lines
to get the required variants and compose it into product of \texttt{Station}.
% The following table summarizes the mapping of MPL elements into source code
% artifacts:

%\begin{table}[h]
	%\begin{tabular}[t]{|p{2.4cm}|p{1.8cm}|p{3.04cm}|}
        %\hline \centering \textbf{Element Name} & \centering \textbf{Artifact} \\
        %\hline ProductLine & Gradle project \\
        %\hline Uses & Gradle project dependency in a \texttt{build.gradle} \\
        %\hline Expose & Public Java class \\
        %\hline
    %\end{tabular}
    %\caption{Map of MPL elements to source code artifact} %
    %\label{tab:uml-mdop-artifact}
%\end{table}
%\vspace{-3em}

% TODO: Kurangi penyebutan contoh, fokus saja langsung ke solusinya
% E.g. to solve depedency issue found in SwitchLine, we include core module
% of signal PL as a compile-time dependency for building the core module of switch

% As for \texttt{SwitchLine}, the steps are similar to how \texttt{SignalLine} is
% initialized. However, there is a dependency issue at an interface (i.e. %% %%
% \texttt{Switch}) that depends on another interface (i.e. \texttt{Sig}) declared
% in \texttt{SignalLine}. Our current approach for resolving this issue is by
% including \texttt{signalpl-core} as a compile-time dependency in Gradle build
% script for building the core module of product line Switch (i.e. \texttt{switchpl-core}).

% Finally, we create \texttt{StationLine} that comprises three Gradle subprojects
% for representing the core module and two delta modules for \texttt{Notification}
% and \texttt{Schedule} features. The core module contains Java classes that
% represent core product, base decorator components, and the factory class. A Java main
% class for demonstrating product configuration and generation in \texttt{StationLine}
% is also created in core module.
