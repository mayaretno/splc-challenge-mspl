package rse.stationpl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import rse.signalpl.SignalFactory;
import rse.signalpl.core.Sig;
import rse.stationpl.core.Notification;
import rse.stationpl.core.Schedule;
import rse.switchpl.SwitchFactory;
import rse.switchpl.core.Sw;

public class Station {

    private static Logger LOGGER = Logger.getLogger(Station.class.getName());

    private List<Sig> signals;
    private List<Sw> switches;
    private List<Notification> notifications;
    private List<Schedule> schedules;

    public Station() {
        signals = new ArrayList<>();
        switches = new ArrayList<>();
        notifications = new ArrayList<>();
        schedules = new ArrayList<>();
    }

    public void addSignal(Sig signal) {
        signals.add(signal);
        LOGGER.info("Added signal with type: " + signal.getClass().getName());
    }

    public void addSwitches(Sw aSwitch) {
        switches.add(aSwitch);
        LOGGER.info("Added switch with type: " + aSwitch.getClass().getName());
    }

    public void addNotification(Notification notification) {
        notifications.add(notification);
        LOGGER.info("Added notification");
    }

    public void addSchedule(Schedule schedule) {
        schedules.add(schedule);
        LOGGER.info("Added schedule");
    }

    public static void main(String[] args) {
        Station stationA = createStationA(); // StationA product
        Station stationB = createStationB(); // StationB product
    }

    private static Station createStationA() {
        LOGGER.info("Creating StationA...");
        Sig s1 = SignalFactory.createSignal("rse.signalpl.mainsig.Signal",
            SignalFactory.createSignal()); //fitur MainSig
        Sig s2 = SignalFactory.createSignal("rse.signalpl.presig.Signal",
            SignalFactory.createSignal(), s1); //fitur PreSig
        Sw sw1 = SwitchFactory.createSwitch("rse.switchpl.manual.Switch",
            SwitchFactory.createSwitch()); //fitur manual switch
        sw1.register(s1);
      
        Station stationA = new Station();
        stationA.addSignal(s1);
        stationA.addSignal(s2);
        stationA.addSwitches(sw1);
        stationA.addNotification(new Notification("This is a test"));

        return stationA;
    }

    private static Station createStationB() {
        LOGGER.info("Creating StationB...");
        Sig s1 = SignalFactory.createSignal("rse.signalpl.mainsig.Signal",
            SignalFactory.createSignal()); //fitur MainSig
        Sig s2 = SignalFactory.createSignal("rse.signalpl.presig.Signal",
            SignalFactory.createSignal(), s1); //fitur PreSig
        Sw sw1 = SwitchFactory.createSwitch("rse.switchpl.electric.Switch",
            SwitchFactory.createSwitch()); //fitur electric switch
        sw1.register(s1);

        Station stationB = new Station();
        stationB.addSignal(s1);
        stationB.addSignal(s2);
        stationB.addSwitches(sw1);
        stationB.addSchedule(new Schedule());

        return stationB;
    }
}
