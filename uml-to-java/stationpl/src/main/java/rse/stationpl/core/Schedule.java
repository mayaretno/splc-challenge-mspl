package rse.stationpl.core;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Schedule {

    private static Logger LOGGER = Logger.getLogger(Schedule.class.getName());

    private List<String> timetable;

    public Schedule() {
        this.timetable = new ArrayList<>();
        LOGGER.info("Created a Schedule with empty timetable");
    }
}
