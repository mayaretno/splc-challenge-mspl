package rse.stationpl.core;

import java.util.logging.Logger;

public class Notification {

    private static Logger LOGGER = Logger.getLogger(Notification.class.getName());

    private final String message;

    public Notification(String message) {
        this.message = message;
        LOGGER.info("Created a Notification with message: " + message);
    }
}
