package rse.signalline.core;

public class SignalImpl implements ISignal {

    private RunBehaviour runBehaviour;

    public SignalImpl() {
        this(new CoreRunBehaviour());
    }

    public SignalImpl(RunBehaviour runBehaviour) {
        this.runBehaviour = runBehaviour;
        runBehaviour.run();
    }
}
