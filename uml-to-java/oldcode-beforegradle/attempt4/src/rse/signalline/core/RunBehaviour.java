package rse.signalline.core;

public abstract class RunBehaviour {

    public void run() {
        before();
        main();
        after();
    }

    protected abstract void before();

    protected abstract void main();

    protected abstract void after();
}
