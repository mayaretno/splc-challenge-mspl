package rse.signalline.core;

import java.util.logging.Logger;

public class CoreRunBehaviour extends RunBehaviour {

    private static final Logger LOGGER = Logger.getLogger(CoreRunBehaviour.class.getName());

    @Override
    protected void before() {
        // Do nothing
    }

    @Override
    protected void main() {
        LOGGER.info("Created CoreRunBehaviour used in core Signal");
    }

    @Override
    protected void after() {
        // Do nothing
    }
}
