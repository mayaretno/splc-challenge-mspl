package rse.signalline.DMainSig;

import rse.signalline.core.RunBehaviour;

import java.util.logging.Logger;

public class RunMainSignalBehaviour extends RunBehaviour {

    private static final Logger LOGGER = Logger.getLogger(RunMainSignalBehaviour.class.getName());

    @Override
    protected void before() {
        // Do nothing
    }

    @Override
    protected void main() {
        LOGGER.info("Created RunMainSignalBehaviour used in variant of MainSignal");
    }

    @Override
    protected void after() {
        // Do nothing
    }
}
