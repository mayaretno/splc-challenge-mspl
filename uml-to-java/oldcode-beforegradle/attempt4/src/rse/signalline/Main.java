package rse.signalline;

import rse.signalline.DMainSig.RunMainSignalBehaviour;
import rse.signalline.core.ISignal;
import rse.signalline.core.SignalImpl;

import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());
    private static String signalProd = "mainsig";

    public static void main(String[] args) {
        LOGGER.info("Start SPL - Strategy example");

        ISignal coreSignal = new SignalImpl(); // Core
        ISignal mainSignal = new SignalImpl(new RunMainSignalBehaviour()); // MainSignal

        LOGGER.info("Finish SPL - Strategy example");
    }
}
