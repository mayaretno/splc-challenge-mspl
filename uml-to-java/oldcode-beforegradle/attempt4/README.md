# Fourth Attempt

This example uses Strategy & Template Method patterns to simulate
DOP-like behaviour modification.

## Notes

Chicken-and-egg problem when instantiating product (i.e. object). What
comes first? The behaviour or the product?

This example assumes behaviour of `run()` method is independent to the
object that uses it. In other words, the behaviour does not depend on
the object that use the behaviour. This will be an issue if the behaviour
actually require information from the dependent object.
