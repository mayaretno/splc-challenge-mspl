package rse.strategy;

/**
 * Coba menerapkan strategy pattern untuk membolehkan pergantian
 * implementasi method run dengan delegasi
 */
public class SignalImpl implements Sig {

    private RunBehaviour runBehaviour;

    public void run() {
        runBehaviour.runImpl();
    }

    public void setRunBehaviour(RunBehaviour runBehaviour) {
        this.runBehaviour = runBehaviour;
    }
}
