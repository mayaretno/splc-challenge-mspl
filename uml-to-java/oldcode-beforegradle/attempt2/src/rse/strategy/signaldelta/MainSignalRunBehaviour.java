package rse.strategy.signaldelta;

import rse.strategy.RunBehaviour;

public class MainSignalRunBehaviour extends RunBehaviour {
    @Override
    public void runImpl() {
        System.out.println("Created main signal");
    }
}
