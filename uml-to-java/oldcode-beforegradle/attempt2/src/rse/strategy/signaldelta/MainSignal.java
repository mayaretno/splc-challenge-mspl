package rse.strategy.signaldelta;


import rse.strategy.SignalImpl;

import java.lang.reflect.Method;

/**
 * Percobaan mau mengubah kelas Java menggunakan reflection
 */
public class MainSignal {

    SignalImpl coreSignal;

    public void run() throws NoSuchMethodException {
        Class<?> clz = coreSignal.getClass();
        Method runMethod = clz.getMethod("run");

        // runMethod tidak bisa diubah implementasinya melalui Java Reflection
    }
}
