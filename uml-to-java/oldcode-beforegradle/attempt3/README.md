# Third Attempt

This exaample uses Decorator pattern to simulate DOP-like behaviour 
modification.

## Notes

```
May 17, 2018 11:01:15 AM rse.signalline.Main main
INFO: Start SPL - Decorator example
May 17, 2018 11:01:15 AM rse.signalline.core.SignalImpl run
INFO: Created Signal
May 17, 2018 11:01:15 AM rse.signalline.DMainSig.SignalImpl run
INFO: Created modified Signal by DMainSig
May 17, 2018 11:01:15 AM rse.signalline.Main main
INFO: Finish SPL - Decorator example
```

Recall that `run()` is always invoked during instantiation of concrete
class and decorator pattern require an existing *product* (i.e. object of
`SignalImpl`) to be wrapped. When we tried to create product of `SignalImpl` based on
application of `DMainSig`, we had to create the core product first and
then decorated it with `SignalImpl` from `DMainSig`.

~~We conclude that Decorator pattern cannot emulate DOP mechanism in Java
because the core product is required when instantiating the variant.~~