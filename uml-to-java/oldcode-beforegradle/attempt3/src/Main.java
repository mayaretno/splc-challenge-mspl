import rse.signalline.SignalFactory;
import rse.signalline.core.SignalComponent;

import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());
    private static String signalProd = "presig";

    public static void main(String[] args) {
        LOGGER.info("Start SPL - Decorator example");

        SignalComponent preSig = SignalFactory.createSignal("presig");
        SignalComponent mainSig = SignalFactory.createSignal("mainsig");

        LOGGER.info("Finish SPL - Decorator example");
    }
}
