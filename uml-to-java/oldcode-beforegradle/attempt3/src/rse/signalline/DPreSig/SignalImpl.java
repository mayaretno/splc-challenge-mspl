package rse.signalline.DPreSig;

import rse.signalline.core.SignalComponent;
import rse.signalline.core.SignalDecorator;

import java.util.logging.Logger;

public class SignalImpl extends SignalDecorator {

    private static final Logger LOGGER = Logger.getLogger(SignalImpl.class.getName());

    public SignalImpl(SignalComponent signal) {
        super(signal);
    }

    @Override
    public void run() {
        LOGGER.info("Created modified Signal by DPreSig");
    }

    public static void main(String[] args) {
        SignalComponent coreSig = new rse.signalline.core.SignalImpl();
        SignalComponent preSig = new SignalImpl(coreSig);
    }
}
