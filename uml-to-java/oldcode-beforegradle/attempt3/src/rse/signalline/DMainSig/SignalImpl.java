package rse.signalline.DMainSig;

import rse.signalline.core.SignalComponent;
import rse.signalline.core.SignalDecorator;

import java.util.logging.Logger;

public class SignalImpl extends SignalDecorator {

    private static final Logger LOGGER = Logger.getLogger(SignalImpl.class.getName());

    public SignalImpl(SignalComponent signal) {
        super(signal);
    }

    @Override
    public void run() {
        LOGGER.info("Created modified Signal by DMainSig");
    }

    public static void main(String[] args) {
        SignalComponent core = new rse.signalline.core.SignalImpl();
        SignalImpl signal = new SignalImpl(core);
    }
}
