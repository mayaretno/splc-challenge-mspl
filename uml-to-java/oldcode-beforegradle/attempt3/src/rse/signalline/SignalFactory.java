package rse.signalline;

import rse.signalline.core.SignalComponent;
import rse.signalline.core.SignalImpl;

public class SignalFactory {

    private SignalFactory() { }

    public static SignalComponent createSignal(String productType) {
        SignalComponent signal = new SignalImpl();

        switch(productType) {
            case "presig":
                signal = new rse.signalline.DPreSig.SignalImpl(signal);
                break;
            case "mainsig":
                signal = new rse.signalline.DMainSig.SignalImpl(signal);
                break;
        }

        return signal;
    }
}
