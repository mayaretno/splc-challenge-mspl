package rse.signalline.core;

public abstract class SignalDecorator extends SignalComponent {

    private SignalComponent signal;

    public SignalDecorator(SignalComponent signal) {
        this.signal = signal;
    }

    public abstract void run();
}
