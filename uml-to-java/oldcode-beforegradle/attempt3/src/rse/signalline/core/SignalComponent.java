package rse.signalline.core;

public abstract class SignalComponent implements ISignal {

    public SignalComponent() {
        run();
    }

    public abstract void run();
}
