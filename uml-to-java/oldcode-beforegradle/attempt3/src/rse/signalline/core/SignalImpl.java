package rse.signalline.core;

import java.util.logging.Logger;

public class SignalImpl extends SignalComponent {

    private static final Logger LOGGER = Logger.getLogger(SignalImpl.class.getName());

    @Override
    public void run() {
        LOGGER.info("Created Signal");
    }
}
