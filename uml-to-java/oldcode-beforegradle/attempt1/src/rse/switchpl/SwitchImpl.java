package rse.switchpl;

import rse.signalpl.Signal;

import java.util.ArrayList;
import java.util.List;

public class SwitchImpl implements ISwitch {

    private List<Signal> signals = new ArrayList<>();

    public SwitchImpl() {
        System.out.println("Created switch");
    }

    @Override
    public void register(Signal signal) {
        signals.add(signal);
    }
}
