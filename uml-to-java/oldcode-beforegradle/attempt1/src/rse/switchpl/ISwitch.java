package rse.switchpl;

import rse.signalpl.Signal;

public interface ISwitch {

    // Comment: Why refer to a type of concrete class instead of its interface
    // type?
    void register(Signal signal);
}
