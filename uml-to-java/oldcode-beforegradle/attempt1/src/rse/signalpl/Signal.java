package rse.signalpl;

// Signal uses public modifier to ensure it can be imported from different package
public class Signal implements Sig {

    public Signal() {
        run();
    }

    public void run() {
        System.out.println("Created signal");
    }
}
