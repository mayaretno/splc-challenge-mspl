package rse.switchpl.electric;

import rse.switchpl.SwitchFactory;
import rse.switchpl.core.Sw;
import rse.switchpl.core.SwitchDecorator;

public class Switch extends SwitchDecorator {

    public Switch(Sw sw) {
        super(sw);
    }

    @Override
    public void run() {
        System.out.println("Created Switch (Electric)");
    }

    public static void main(String[] args) {
        Sw core = SwitchFactory.createSwitch();
        Sw electric = SwitchFactory.createSwitch(Switch.class.getName(), core);
    }
}
