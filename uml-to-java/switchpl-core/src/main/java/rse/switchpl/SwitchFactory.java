package rse.switchpl;

import java.lang.reflect.Constructor;
import java.util.logging.Logger;
import rse.switchpl.core.Sw;
import rse.switchpl.core.Switch;

public class SwitchFactory {

    private static final Logger LOGGER = Logger.getLogger(SwitchFactory.class.getName());

    private SwitchFactory() {
        // Default private constructor
    }

    public static Sw createSwitch() {
        return new Switch();
    }

    public static Sw createSwitch(String fullyQualifiedName, Object... base) {
        Sw aSwitch = (base.length == 0) ? null : (Sw) base[0];

        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
            aSwitch = (Sw) constructor.newInstance(base);
        } catch (Exception ex) {
            LOGGER.severe("Failed to create instance of Switch. Returning base Switch.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
        }

        return aSwitch;
    }
}
