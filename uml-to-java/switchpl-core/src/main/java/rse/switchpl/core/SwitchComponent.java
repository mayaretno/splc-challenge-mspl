package rse.switchpl.core;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import rse.signalpl.core.Sig;

public abstract class SwitchComponent implements Sw {

    private static final Logger LOGGER = Logger.getLogger(SwitchComponent.class.getName());

    private List<Sig> signals;

    public SwitchComponent() {
        signals = new ArrayList<>();
        run();
    }

    @Override
    public void register(Sig sig) {
        signals.add(sig);
        LOGGER.info("Registered signal with type: " + sig.getClass().getName());
    }

    public abstract void run();
}
