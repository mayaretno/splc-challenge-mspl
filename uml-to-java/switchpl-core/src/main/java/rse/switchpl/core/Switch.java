package rse.switchpl.core;

import rse.signalpl.SignalFactory;
import rse.signalpl.core.Sig;
import rse.switchpl.SwitchFactory;

public class Switch extends SwitchComponent {

    @Override
    public void run() {
        System.out.println("Created Switch (Core)");
    }

    public static void main(String[] args) {
        Sig signal = SignalFactory.createSignal();
        Sw aSwitch = SwitchFactory.createSwitch(Switch.class.getName());
        aSwitch.register(signal);
    }
}
