package rse.switchpl.core;

import rse.signalpl.core.Sig;

/**
 * Base type for {@code Switch} class.
 */
public interface Sw {

    void register(Sig sig);
}
