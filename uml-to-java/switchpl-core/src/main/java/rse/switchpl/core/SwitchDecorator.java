package rse.switchpl.core;

public abstract class SwitchDecorator extends SwitchComponent {

    protected Sw sw;

    public SwitchDecorator(Sw sw) {
        this.sw = sw;
    }

    @Override
    public abstract void run();
}
