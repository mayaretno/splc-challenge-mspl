package rse.signalpl.mainsig;

import rse.signalpl.SignalFactory;
import rse.signalpl.core.Sig;
import rse.signalpl.core.SignalComponent;
import rse.signalpl.core.SignalDecorator;

public class Signal extends SignalDecorator {

    public Signal(SignalComponent signal) {
        super(signal);
    }

    @Override
    public void run() {
        System.out.println("Created Signal (Main Signal)");
    }

    public static void main(String[] args) {
        Sig base = SignalFactory.createSignal();
        Sig mainSig = SignalFactory.createSignal(Signal.class.getName(), base);
    }
}
