package rse.switchpl.manual;

import rse.switchpl.SwitchFactory;
import rse.switchpl.core.Sw;
import rse.switchpl.core.SwitchComponent;
import rse.switchpl.core.SwitchDecorator;

public class Switch extends SwitchDecorator {

    public Switch(SwitchComponent sw) {
        super(sw);
    }

    @Override
    public void run() {
        System.out.println("Created Switch (Manual)");
    }

    public static void main(String[] args) {
        Sw core = SwitchFactory.createSwitch();
        Sw manual = SwitchFactory.createSwitch(Switch.class.getName(), core);
    }
}
