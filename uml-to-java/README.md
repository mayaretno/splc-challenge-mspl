# SPLC Challenge - UML to Java

This document contains notes about mapping elements from UML-DOP into Java
classes. The UML-DOP diagram models **multi software product line (MPL)**
problem where there is a need for combining products from separate PL and
lets a component in a PL exposed to other product.

## UML-DOP Elements

The following is list of UML-DOP elements found in the given diagram:

- Ordinary UML classes in Switch PL
    - `<<interface>> Switch` --> `interface ISwitch`
    - `Switch` --> `class SwitchImpl implements ISwitch`
        > Note 1: `run` method always executed when object is instantiated.
        > This behaviour is mapped to constructor method in Java.
        > Note 2: `register` method requires an argument with type `Signal`
        > that actually present in separate PL (in Signal PL)
- `<<modifies>>` relationship between `Switch` and `<<delta>>` packages
(Switch PL)
    - Relation edge between `Switch` and `<<delta>>` package represents
    changes that will be applied to `Switch` by stereotyped UML classes
    in the package
    - Deltas are contained in stereotyped UML Package and represented as
    stereotyped UML class elements
    - Still a big question on how to represent relation between class and
    its delta in Java
        - Ideas: Java 9 module system? Dependency injection (ala Spring
        autowiring)?
- Stereotyped UML package
    - Contains stereotype UML Classes that represent deltas
    - Two possible translation in Java: package or module (Java 9)
- Stereotyped UML classes in the delta package
    - Delta contains changes that will be applied to a method in targeted
    class
    - Possible translation in Java
        - Decorator pattern --> all Java classes that can be overridden
        via delta need to have `Component` and `Decorator` abstraction,
        and the deltas are represented as a concrete type of Decorator
            > ~~Comment: Will result in too many boilerplate; probably
            > better to use AOP instead?~~ Not that many
        - Implement as ordinary classes that actually type of `Signal`
        (or a targeted class) whose implementation has been modified
        by the delta
- Stereotyped (`<<productline>>`) UML component diagram
    - Contains list of features, products, and (possibly?) product derivation
    rules (e.g. how the deltas will be applied when generating a product)
    - Need to think of how generating product can be conducted manually
        - Manually (described as pseudocode)?
        - Build automation system, e.g. Gradle?
        - Bash scripting?
- Ordinary UML classes in Signal PL
    - `<<interface>> Sig` --> `interface Sig`
    - `Signal` --> `class Signal implements Sig`
        > Note 1: `Signal` type is required by `SwitchImpl` in separate PL
        > (in Switch PL).
        > Note 2: Same as `Switch` Java class, there is `run` method that
        > always invoked when the object is instantiated.
        
## Notes

- Even though deltas can be used to add, remove, and modify a class, the
challenge case only provide example where the delta modify a method/behaviour
present in a class
    - The translation to Java will only cover this delta application
    scenario
- Remember that deltas can be applied in certain order (defined in product
derivation)
