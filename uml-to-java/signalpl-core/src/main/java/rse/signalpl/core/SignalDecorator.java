package rse.signalpl.core;

public abstract class SignalDecorator extends SignalComponent {

    protected SignalComponent signal;

    public SignalDecorator(SignalComponent signal) {
        this.signal = signal;
    }

    @Override
    public abstract void run();
}
