package rse.signalpl.core;

import rse.signalpl.SignalFactory;

public class Signal extends SignalComponent {

    @Override
    public void run() {
        System.out.println("Created Signal (Core)");
    }

    public static void main(String[] args) {
        SignalFactory.createSignal(Signal.class.getName());
    }
}
