package rse.signalpl.core;

public abstract class SignalComponent implements Sig {

    public SignalComponent() {
        run();
    }

    public abstract void run();
}
