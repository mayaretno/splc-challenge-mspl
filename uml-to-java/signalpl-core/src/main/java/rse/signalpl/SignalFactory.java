package rse.signalpl;

import java.lang.reflect.Constructor;
import java.util.logging.Logger;

import rse.signalpl.core.Sig;
import rse.signalpl.core.Signal;

public class SignalFactory {

    private static final Logger LOGGER = Logger.getLogger(SignalFactory.class.getName());

    private SignalFactory() {
        // Default private constructor
    }

    public static Sig createSignal() {
        return new Signal();
    }

    public static Sig createSignal(String fullyQualifiedName, Object... base) {
        Sig signal = (base.length == 0) ? null : (Sig) base[0];

        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
            signal = (Sig) constructor.newInstance(base);
        } catch (Exception ex) {
            LOGGER.severe("Failed to create instance of Sig. Returning base Signal.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
        }

        return signal;
    }
}
