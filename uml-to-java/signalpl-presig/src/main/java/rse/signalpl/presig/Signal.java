package rse.signalpl.presig;

import rse.signalpl.SignalFactory;
import rse.signalpl.core.Sig;
import rse.signalpl.core.SignalComponent;
import rse.signalpl.core.SignalDecorator;

public class Signal extends SignalDecorator {

    private rse.signalpl.mainsig.Signal mainSignal;

    public Signal(SignalComponent signal, rse.signalpl.mainsig.Signal mainSignal) {
        super(signal);
        this.mainSignal = mainSignal;
    }

    @Override
    public void run() {
        System.out.println("Created Signal (Pre Signal)");
    }

    public static void main(String[] args) {
        Sig mainSig = SignalFactory.createSignal("rse.signalpl.mainsig.Signal",
            SignalFactory.createSignal());
        Sig preSig = SignalFactory.createSignal(Signal.class.getName(),
            SignalFactory.createSignal(), mainSig);
    }
}
