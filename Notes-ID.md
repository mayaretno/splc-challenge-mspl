## Outline Paper

1. Motivation + Idea (0,5)
2. UML-Java (2)
Pseudocode- Ekstension UMLDOP - UML Diagram - Java
3. Evaluation (1)
4. Concluding Remarks and Discussion (0,5)

## Timeline

- 11 Mei: Sect 2 selesai bahas evaluasi
- 18 Mei: Evaluasi Sect 3 dan Revise Sect 2 bila perlu
- 22 Mei: Finalisasi Paper --> Sect 1 dan 4

## Notes

> These notes are written in Bahasa Indonesia

### May 24

Daya baru baca ulang paper challenge case. Beberapa poin penting yang Daya
catat:

- Sebuah produk sinyal dalam perkeretaapian dapat memiliki berbagai variasi
    - Dalam bahasa product line: product line Signal dapat mengandung beberapa
    varian produk Signal, e.g. `PreSignal`, `MainSignal`
- Ilustrasi `PreSignal` memiliki dependency terhadap `MainSignal` menggambarkan
kemungkinan interaksi yang dapat terjadi pada sebuah varian produk di suatu
product line
    - Dalam paper challenge case, diceritakan `PreSignal` semacam memanggil
    operasi yang dimiliki `MainSignal`
- Stasiun kereta api pun juga dapat memiliki variasi, misal: stasiun terminus
(e.g. Stasiun Bogor) dan stasiun sentral (e.g. Manggarai), dimana tiap varian
stasiun bisa memiliki kombinasi produk Signal dan Switch yang bermacam-macam
    - Variasi Signal di sebuah stasiun --> *Multiple variants from a product line*
- Isu utama yang diangkat paper challenge case: *Multiple variants from a product
line in an application*
    - Daya menarik kesimpulan bahwa pertanyaan utama dari challenge case ini
    adalah: *How to handle multiple variants of one product line?*
- Aspek modularitas sepertinya cukup ditekankan di challenge case, namun Daya
masih kurang mengerti (belum dapat menjelaskannya dengan bahasa sendiri)
- Design challenges (disebutkan juga di dalam paper challenge case)
    - Interoperability antara varian produk dari sebuah product line di dalam
    sebuah aplikasi
    - Bagaimana sebuah product line membuka (expose) varian produknya ke
    aplikasi yang membutuhkan atau ke product line lain
    - Bagaimana menangani *name clash* ketika ada 2 varian yang mengandung
    elemen yang sama

### May 18

Alternatif product configuration:

1. Level sederhana Methodnya Station create object Signal
2. Level tengah2, product configuration
- Script mengatur multi product configuration --> mengacu ke Ina Schaefer
- Perbandingan dengan appoach paper multiSPL
3. Level kompleks pakai GUI 

- CO Existence tidak masalah harusnya.
- Reference masih jadi issue
- Exposing bebas di Java seperti apa asal tidak error
- Expected OUTPUT sesuai dengan source code awal asal tidak error

### May 5

Opsi:

1. Simple inheritance
2. Strategic Pattern
Delegation
3. Decorator Pattern

Library diassume ada

Assuming,
Productline --> Gradle
Product --> Subproject
Ide dipending

Manual Pakai Jar
SigCore.jar -> create signal
MainSig.jar -> create main signal

Feature --> 

Delta --> Package
Delta memodifikasi Package core

Siapin masing2 core module
Signal akan diekspose karena butuh switch
Mekanisme expose signal, pakai visibility dari suatu class yang diexpose oleh package

