# SPLC Challenge Case: Interoperability of SPL Variants

Reliable Software Engineering Lab, Faculty of Computer Science Universitas
Indonesia

[![pipeline status](https://gitlab.com/mayaretno/splc-challenge-mspl/badges/master/pipeline.svg)](https://gitlab.com/mayaretno/splc-challenge-mspl/commits/master)

* * *

This project repository contains the paper and solution artifact for a challenge
case in SPLC 2018. The challenge case is about interoperability of variants in
a Multi Software Product Lines (MPL). We try to solve the challenge case by
refactoring the challenge case to UML model using UML-DOP profile. UML model
is transformed to solution artifact that consists of Gradle projects and Java
source code files. The transformation from model to solution artifact is done
pragmatically with addition of our own proposed rules in mapping MPL elements
into solution artifact.

The following table summarizes the idea for mapping the MPL elements to solution
artifact in Java/Gradle:

| Element     | Artifact                                                                   |
|-------------|----------------------------------------------------------------------------|
| ProductLine | Gradle project (including a build script, i.e. `build.gradle`)             |
| Uses        | Gradle project dependency, e.g. `implementation project(':switchpl-core')` |
| Expose      | Java class with public visibility modifier                                 |

We currently do the transformation manually and will look up for the possibility
to formalize and automate the transformation in the future.

The running example is stored in [`uml-to-java`](uml-to-java/) directory. To run
the simulation, execute the following command in the directory using your
favorite shell:

```bash
$ ./gradlew check :stationpl:run
```

To generate PDF document of the challenge case report, execute:

```bash
$ latex acmart.ins
$ latexmk -pdf sample-sigconf.tex
$ qpdf --linearize sample-sigconf.pdf sample-sigconf-web.pdf
```

> Note: If you have not installed `qpdf` in your system, you can do so by
> invoking `apt-get install qpdf` in a modern Ubuntu distribution.

## Directory Structure

- [UMLDiagram](UMLDiagram/) -- contains images of proposed UML diagram
- [abscode](abscode/) -- contains the proposed pseudocode written in ABS
language
- [uml-to-java](uml-to-java/) -- contains the solution artifact for
demonstrating the pseudocode implementation using Java and Gradle

## Authors

- [Maya Retno Ayu Setyautami](https://gitlab.com/mayaretno)
- [Daya Adianto](https://gitlab.com/addianto)
- [Ade Azurat](https://gitlab.com/adeazurat)
